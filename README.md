# VDJServer IgBlast #

Primary purpose of this repository is to build a docker image for IgBlast with the VDJServer tools and germline database. Currently VDJServer does not use this docker image for running IgBlast on the supercomputer. The docker images is useful for testing and running tools on your local computer.
