# This builds a composite image with IgBlast, VDJML
# and repertoire-summarization. Useful for testing.

# Base Image
FROM vdjserver/vdjml:v1.0.2

MAINTAINER VDJServer <vdjserver@utsouthwestern.edu>

# PROXY: uncomment these if building behind UTSW proxy
#ENV http_proxy 'http://proxy.swmed.edu:3128/'
#ENV https_proxy 'https://proxy.swmed.edu:3128/'
#ENV HTTP_PROXY 'http://proxy.swmed.edu:3128/'
#ENV HTTPS_PROXY 'https://proxy.swmed.edu:3128/'

# Install OS Dependencies
RUN apt-get update && apt-get install -y \
    vim emacs \
    cpio

# Igblast
ENV IGBLAST_VERSION 1.14.0
RUN wget ftp://ftp.ncbi.nih.gov/blast/executables/igblast/release/$IGBLAST_VERSION/ncbi-igblast-$IGBLAST_VERSION-x64-linux.tar.gz

RUN tar zxf ncbi-igblast-$IGBLAST_VERSION-x64-linux.tar.gz
ENV PATH /ncbi-igblast-$IGBLAST_VERSION/bin:$PATH
